package co.gov.coldeportes.deportealamano.dominio.entidades;

public class Evento {

	private String nombre;
	private String tipo;
	private String fechaD;
	private String fechaH;
	private String pais;
	private String lugar;
	private String territorio;
	private String entidad;
	private String escenario;
	private String descripEvento;
	private String paginaWeb;
	private String email;	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getFechaD() {
		return fechaD;
	}
	public void setFechaD(String fechaD) {
		this.fechaD = fechaD;
	}
	public String getFechaH() {
		return fechaH;
	}
	public void setFechaH(String fechaH) {
		this.fechaH = fechaH;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getLugar() {
		return lugar;
	}
	public void setLugar(String lugar) {
		this.lugar = lugar;
	}
	public String getTerritorio() {
		return territorio;
	}
	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}
	public String getEntidad() {
		return entidad;
	}
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}
	public String getEscenario() {
		return escenario;
	}
	public void setEscenario(String escenario) {
		this.escenario = escenario;
	}
	public String getDescripEvento() {
		return descripEvento;
	}
	public void setDescripEvento(String descripEvento) {
		this.descripEvento = descripEvento;
	}
	public String getPaginaWeb() {
		return paginaWeb;
	}
	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	
	
}
